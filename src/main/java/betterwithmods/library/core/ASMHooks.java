package betterwithmods.library.core;

import betterwithmods.library.event.EntityDropEquipmentEvent;
import betterwithmods.library.event.EntitySetEquipmentEvent;
import betterwithmods.library.event.StructureSetBlockEvent;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;
import net.minecraft.world.gen.structure.StructureComponent;
import net.minecraftforge.common.MinecraftForge;

public class ASMHooks {

    public static final String HOOKS = "betterwithmods/library/core/ASMHooks";


    public static boolean onEntitySetEquipment(Entity entity, DifficultyInstance difficultyInstance) {
        return MinecraftForge.EVENT_BUS.post(new EntitySetEquipmentEvent(entity, difficultyInstance));
    }

    public static boolean onEntityDropEquipment(Entity entity, boolean wasRecentlyHit, int lootingModifier) {
        return MinecraftForge.EVENT_BUS.post(new EntityDropEquipmentEvent(entity, wasRecentlyHit, lootingModifier));
    }

    public static IBlockState onStructureSetBlock(StructureComponent component, World world, BlockPos pos, int x, int y, int z, IBlockState state) {
        StructureSetBlockEvent event = new StructureSetBlockEvent(component, world, pos, new BlockPos(x, y, z), state);
        MinecraftForge.EVENT_BUS.post(event);
        return event.getState();
    }
}
